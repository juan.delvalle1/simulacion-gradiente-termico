#include <elipsecolor.h>

ConicalColor::ConicalColor(int x, int y, int h, int w) :
    QGraphicsEllipseItem(x,y,h,w)
{
    this->setBrush(QBrush(Qt::black));
    this->DirAngle= 0;
}

void ConicalColor::changeDireccionX(int DirX){
    this->DirX= DirX;
    updateColor();

}

void ConicalColor::changeDireccionY(int DirY){
    this->DirY = DirY;
    updateColor();
}

void ConicalColor::changeAngle(int DirX, int DirY){
    double h= sqrt(pow(DirX, 2) + pow(DirY,2));
    double v= DirY/h;
    double DirAngle= asin(v);
    this->DirAngle= DirAngle;
    updateColor();
}

void ConicalColor::updateColor(){
    QConicalGradient conicalGradient(QPointF(this->DirX, this->DirY),this->DirAngle);
    conicalGradient.setColorAt(0, Qt::red);
    conicalGradient.setColorAt(0.125,Qt::yellow);
    conicalGradient.setColorAt(0.25, Qt::green);
    conicalGradient.setColorAt(0.375,Qt::cyan);
    conicalGradient.setColorAt(0.5,Qt::blue);
    conicalGradient.setColorAt(0.625,Qt::cyan);
    conicalGradient.setColorAt(0.75, Qt::green);
    conicalGradient.setColorAt(0.875,Qt::yellow);
    conicalGradient.setColorAt(1,Qt::red);
    this->setBrush(QBrush(conicalGradient));
}
double ConicalColor::getPuntoX()
{
    return dfdx;
}

double ConicalColor::getPuntoY()
{
    return dfdy;
}

void ConicalColor::setPuntoX(int puntoX)
{
    this->dfdx = puntoX;
}

void ConicalColor::setPuntoY(int puntoY)
{
    this->dfdy= puntoY;
}

double ConicalColor::getDireccionX()
{
    return DirX;
}

double ConicalColor::getDireccionY()
{
    return DirY;
}

void ConicalColor::setDireccionX(int direccionX)
{
    this->DirX= direccionX;
    updateColor();
}

void ConicalColor::setDireccionY(int direccionY)
{
    this->DirY= direccionY;
    updateColor();
}

void ConicalColor::temperatura(F<double> x, F<double> y)
{
    F<double> z=pow(x,3);
    F<double> p= z/5;
    F<double> r=pow(y,5);
    F<double> s= r/3;
    F<double> T=9*(s+p);

    x.diff(0,2); //diferenciacion de la funcion 0 y 2 denotan los index
    y.diff(1,2);
    double dfdx=T.d(0);//derivada parcial de x
    double dfdy=T.d(1);//derivada parcial de y
    double px= x.x();
    double py= y.x();
    this->setPuntoX(int(px));
    this->setPuntoY(int(py));
    this->setDireccionX(int(dfdx));
    this->setDireccionY(int(dfdy));
    this->changeAngle(int(dfdx),int(dfdy));
    updateColor();
}
