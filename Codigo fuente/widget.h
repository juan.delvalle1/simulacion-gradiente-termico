#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QGraphicsScene>
#include <QObject>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QVBoxLayout>
#include "rectanglecolor.h"
#include "circlecolor.h"
#include "elipsecolor.h"
namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
public slots:
  void dialValueChanged(int value);
  void on_dialVerde_valueChanged(int value);
  void on_dialRojo_2_valueChanged(int value);
  void on_dialVerde_2_valueChanged(int value);
  void on_dialRojo_3_valueChanged(int value);
  void on_dialVerde_3_valueChanged(int value);

public:
    Ui::Widget *ui;
    QGraphicsScene *scene1;
    QGraphicsScene *scene2;
    QGraphicsScene *scene3;
    RectangleColor *rect;
    CircleColor *circle;
    ConicalColor *cone;



    //void on_ui_windowTitleChanged(const QString &title);
private slots:

};

#endif // WIDGET_H
