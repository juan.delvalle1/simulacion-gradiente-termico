#ifndef RECTANGLECOLOR_H
#define RECTANGLECOLOR_H

#include <QGraphicsRectItem>
#include <QObject>
#include <QPainter>
#include <QtCore>
#include <QtGui>
#include <QBrush>
#include <QPen>
#include <QColor>
#include <QPaintEvent>
#include <QLinearGradient>
#include "fadbad.h"
#include "fadiff.h"
#include "tadiff.h"

using namespace fadbad;

class RectangleColor:public QObject,public QGraphicsRectItem{
    Q_OBJECT
public:
    RectangleColor(int x, int y, int w, int h);
    F<double> x;
    F<double> y;

signals:

public slots:
   void changeDireccionX(int DirX);

   void changeDireccionY(int DirY);

    double getPuntoX();

    double getPuntoY();

    void setPuntoX(int puntoX);

    void setPuntoY(int puntoY);

    double getDireccionX();

    double getDireccionY();

    void setDireccionX(int direccionX);

    void setDireccionY(int direccionY);

    void temperatura(F<double> x, F<double> y);

private:
    void updateColor();
    int dfdx;
    int dfdy;
    int DirX;
    int DirY;
};

#endif // RECTANGLECOLOR_H
