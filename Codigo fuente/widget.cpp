#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    scene1 = new QGraphicsScene(this);
    scene2 = new QGraphicsScene(this);
    scene3 = new QGraphicsScene(this);

    rect = new RectangleColor(0,0,200,200);
    circle= new CircleColor(0,0,200,200);
    cone= new ConicalColor(0,0,200,200);


    scene1->addItem(rect);
    scene2->addItem(circle);
    scene3->addItem(cone);

    ui->graphicsView->setScene(scene1);
    ui->graphicsView_2->setScene(scene2);
    ui->graphicsView_3->setScene(scene3);

    ui->dialRojo->setMinimum(-400);
    ui->dialRojo->setMaximum(400);
    ui->dialVerde->setMinimum(-400);
    ui->dialVerde->setMaximum(400);
    ui->dialRojo_2->setMinimum(-400);
    ui->dialRojo_2->setMaximum(400);
    ui->dialVerde_2->setMinimum(-400);
    ui->dialVerde_2->setMaximum(400);
    ui->dialRojo_3->setMinimum(-400);
    ui->dialRojo_3->setMaximum(400);
    ui->dialVerde_3->setMinimum(-400);
    ui->dialVerde_3->setMaximum(400);


    connect(ui->dialRojo, SIGNAL(valueChanged(int)), this , SLOT(dialValueChanged(int)));
    connect(ui->dialVerde, SIGNAL(valueChanged(int)), this , SLOT(on_dialVerde_valueChanged(int)));
    connect(ui->dialRojo,SIGNAL(valueChanged(int)),rect,SLOT(changeDireccionX(int)));
    connect(ui->dialVerde,SIGNAL(valueChanged(int )),rect,SLOT(changeDireccionY(int)));
    connect(ui->dialRojo_2,SIGNAL(valueChanged(int)),this , SLOT(on_dialRojo_2_valueChanged(int)));
    connect(ui->dialVerde_2,SIGNAL(valueChanged(int)),this , SLOT(on_dialVerde_2_valueChanged(int)));
    connect(ui->dialRojo_2,SIGNAL(valueChanged(int)),circle , SLOT(changeDireccionX(int)));
    connect(ui->dialVerde_2,SIGNAL(valueChanged(int)),circle , SLOT(changeDireccionY(int)));
    connect(ui->dialRojo_3,SIGNAL(valueChanged(int)),this , SLOT(on_dialRojo_3_valueChanged(int)));
    connect(ui->dialVerde_3,SIGNAL(valueChanged(int)),this , SLOT(on_dialVerde_3_valueChanged(int)));
    connect(ui->dialRojo_3,SIGNAL(valueChanged(int)),cone , SLOT(changeDireccionX(int)));
    connect(ui->dialVerde_3,SIGNAL(valueChanged(int)),cone , SLOT(changeDireccionY(int)));
}

void Widget::dialValueChanged(int value)
{

    ui->lcdNumber->display(value);

}

void Widget::on_dialVerde_valueChanged(int value)
{
    ui->lcdNumber_2->display(value);

}


void Widget::on_dialRojo_2_valueChanged(int value)
{
    ui->lcdNumber_3->display(value);
    circle->changeDireccionX(circle->getDireccionX());
    circle->changeDireccionRadial(circle->getDireccionX(),circle->getDireccionY());
}


void Widget::on_dialVerde_2_valueChanged(int value)
{
    ui->lcdNumber_4->display(value);
    circle->changeDireccionRadial(circle->getDireccionX(),circle->getDireccionY());
    circle->changeDireccionY(circle->getDireccionY());

}

void Widget::on_dialRojo_3_valueChanged(int value)
{
    ui->lcdNumber_5->display(value);
    cone->changeAngle(cone->getDireccionX(),cone->getDireccionY());
    cone->changeDireccionX(cone->getDireccionX());
}


void Widget::on_dialVerde_3_valueChanged(int value)
{
    ui->lcdNumber_6->display(value);
    cone->changeAngle(cone->getDireccionX(),cone->getDireccionY());
    cone->changeDireccionX(cone->getDireccionX());
}


Widget::~Widget()
{
    delete ui;
    delete scene1;
    delete scene2;
    delete scene3;
    delete rect;
    delete circle;
    delete cone;
}


