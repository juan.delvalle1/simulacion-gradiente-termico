#include "rectanglecolor.h"

RectangleColor::RectangleColor(int x, int y, int w, int h) :
    QGraphicsRectItem(x,y,w,h)
{
    this->setBrush(QBrush(Qt::black));
}

void RectangleColor::changeDireccionX(int DirX){
    this->DirX= DirX;
    updateColor();

}

void RectangleColor::changeDireccionY(int DirY){
    this->DirY = DirY;
    updateColor();
}

void RectangleColor::updateColor(){
    QLinearGradient linealGradient(QPointF(this->DirX, this->DirY), QPointF(dfdx, dfdy));
    linealGradient.setColorAt(0, Qt::red);
    linealGradient.setColorAt(0.25,Qt::yellow);
    linealGradient.setColorAt(0.5, Qt::green);
    linealGradient.setColorAt(0.75,Qt::cyan);
    linealGradient.setColorAt(1,Qt::blue);
    this->setBrush(QBrush(linealGradient));
}
double RectangleColor::getPuntoX()
{
    return dfdx;
}

double RectangleColor::getPuntoY()
{
    return dfdy;
}

void RectangleColor::setPuntoX(int puntoX)
{
    this->dfdx = puntoX;
}

void RectangleColor::setPuntoY(int puntoY)
{
    this->dfdy= puntoY;
}

double RectangleColor::getDireccionX()
{
    return DirX;
}

double RectangleColor::getDireccionY()
{
    return DirY;
}

void RectangleColor::setDireccionX(int direccionX)
{
    this->DirX= direccionX;
}

void RectangleColor::setDireccionY(int direccionY)
{
    this->DirY= direccionY;
}

void RectangleColor::temperatura(F<double> x, F<double> y)
{
    F<double> z=pow(x,2);
    F<double> r=pow(y,2);
    F<double> T=4*(r+z)-2*r;

    x.diff(0,2); //diferenciacion de la funcion 0 y 2 denotan los index
    y.diff(1,2);
    double dfdx=T.d(0);//derivada parcial de x
    double dfdy=T.d(1);//derivada parcial de y
    double px= x.x();
    double py= y.x();
    this->setPuntoX(int(px));
    this->setPuntoY(int(py));
    this->setDireccionX(int(dfdx));
    this->setDireccionY(int(dfdy));
    updateColor();

}




