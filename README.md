# Gradiente Termico
Gradiente Térmico es un programa que busca simular de manera visual el comportamiento de la temperatura y el calor en una superficie cuando busca el equilibrio térmico. Esto es con fines meramente ilustrativos y no supone un simulador preciso y adecuado.

## Requisitos
QT Creator 5.12.1 o superior
<br>
MinGW o compilador equivalente.
<br>
Libreria FadBAD++
<br>
Para instalar la libreria necesaria para correr el source code se debe ir a la siguiente pagina
<br>
http://www.imm.dtu.dk/~kajm/FADBAD/#Download
<br>
Hacer click en la siguiente linea "Get the sourcecode for FADBAD++ (as zip). "
<br>
Una vez descargado el zip se debe descomprimir y añadir los documentos ahi incluidos a la carpeta donde tenga el codigo de "Gradiente Termico"
guardado.

Una vez instaladas las estas dependencias y librerias neceserias, pasamos a la ejecución

## Compilación
### Linux y Windows

Tras clonar el presente repositorio, abrir el archivo .pro en QTCreator presionar el botón RUN.

Una vez que la pestaña aparece solo se debe mover los diales para cambiar el vector que se esta evaluando y ver como se mueve la temperatura
según los colores siendo rojo el más cálido y azul el más frío.



<br>
Miembros:
<br>
Javiera Alvarez
<br>
Mauricio Carvajal
<br>
Juan Pablo del Valle
<br>
Tomas Garreton
